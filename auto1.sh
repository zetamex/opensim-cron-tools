#/bin/bash

# Search to see if Node_1 is running
# The if statement says if the simulator is running,
# stop there and don't do anything, but if it is not
# not running go and excute our restart file
ps aux | grep '[N]ode_1' | grep -v grep > /dev/null 
if [ $? != 0 ]
then
        /home/regions/restart1.sh > /dev/null
fi
