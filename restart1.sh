#/bin/bash

#This checks for screen instances with the sim's name
#Pay special attention to the [] around the first letter
#that allows us to only get the exact result of what we 
#are looking for
kill $(ps aux | grep '[N]ode_1' | awk '{print $2}')

#Taking a pause for 5 seconds
sleep 5

#Lets start new instance with the simulators name
#Pay special attention that this screen name
#matches the one we are searching to kill
screen -dmS Node_1 ./node1.sh

#Another pause for 5 seconds
sleep 5

#Report back that the simulator is starting
echo "Simulator is restarting, please give at least 2 minutes for logging into it!"
